﻿using System;
#region CLASE ABSTRACTA
//DEFINICION DE CLASE ABSTRACTA
abstract class Figura
{
    //PROPIEDAD ABSTRACTA (DEBE SER IMPLEMENTADA POR CLASES DERIVADAS)
    public abstract double CalcularArea();
    //METODO CONCRETO EN LA CLASE ABSTRACTA
    public void MostrarInformacion()
    {
        Console.WriteLine("Esta es un figura");
    }
}
//CLASE DERIVADA QUE HEREDA DE LA CLASE ABSTRACTA
class Cuadrado : Figura
{
    //IMPLEMENTACION DE LA PROPIEDAD ABSTRACTA
    public override double CalcularArea()
    {
        //IMPLEMENTACION ESPECIFICA PARA EL CUADRADO
        Console.WriteLine("CALCULANDO AREA DEL CUADRADO");
        return 0.0;
    }
}
//CLASE DERIVADA QUE HEREDA DE LA CLASE ABSTRACTA
class Circulo : Figura {
    //IMPLEMENTACION ESPECIFICA PARA EL CIRCULO
    public override double CalcularArea()
    {
        //IMPLEMENTACION ESPECIFICA PARA EL CIRCULO
        Console.WriteLine("CALCULANDO AREA DEL CIRCULO");
        return 0.0;
    }
}
#endregion CLASE ABSTRACTA
#region INTERFAZ
//Definicion de la interfaz
public interface IImprimible
{
    //Metodo que las clases que implementan la interfaz deben proporcionar
    void Imprimir();
}
//clase  que implementa la interfaz
public class Impresora: IImprimible { 
// IMPLEMENTACION DEL METODO DE LA INTERFAZ

    public void Imprimir()
    {
        Console.WriteLine("Imprimiendo el documento");
    }

}
public class Escaner : IImprimible
{

   //IMPLEMENTACION DEL METODO DE LA INTERFAZ
   public void Imprimir()
    {
        Console.WriteLine("Escaneado documento y luego imprimiendo");
    }
}
#endregion INTERFAZ

class Program
{
    static void Main()
    {
        #region ABSTRACTA
        //NO SE PUEDE CREAR UNA INSTANCIA DE LA CLASE ABSTRACTA
        //Figura figura = new Figura(); //Esto dara un error
        //Pero se pueden crear instancias de las clases derivadas.
        Figura cuadrado = new Cuadrado();
        cuadrado.CalcularArea();
        cuadrado.MostrarInformacion();
        Figura circulo = new Circulo();
        circulo.CalcularArea();
        circulo.MostrarInformacion();
        #endregion ABSTRACTA
        #region INTERFAZ
        //CREAR INSTANCIAS DE LAS CLASES QUE IMPLEMENTAN LA INTERFAZ
        Impresora impresora = new Impresora();
        Escaner escaner = new Escaner();    

        //LLAMAR AL METODO DE LA INTERFAZ

        impresora.Imprimir();
        escaner.Imprimir();
        #endregion INTERFAZ


    }
}